<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Task
{
    /**
     * @Assert\GreaterThan(0)
     */
    protected $length;

    /**
     * @Assert\GreaterThan(0)
     */
    protected $quantity;

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}