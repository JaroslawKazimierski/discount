<?php

namespace App\Service;

class DiscountCodes
{
    public function generatedDiscountCodes(int $length, int $quantity)
    {
        $codes = [];
        $pattern = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        for ($i = 0; $i < $quantity; $i++) {
            $code = substr(str_shuffle(str_repeat($pattern, $length )),1, $length);
            while (in_array($code, $codes)) {
                $code = substr(str_shuffle(str_repeat($pattern, $length )),1, $length);
            }
            $codes[] = $code;
        }

        return implode(' , ', $codes);
    }
}