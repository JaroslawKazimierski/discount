<?php

namespace App\Command;

use App\Service\DiscountCodes;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GeneratedCodesCommand extends Command
{
    protected static $defaultName = 'app:generated-codes';

    /** @var ValidatorInterface */
    private $validator;

    /** @var DiscountCodes */
    private $discountCodes;

    public function __construct(
        ValidatorInterface $validator,
        DiscountCodes $discountCodes,
        string $name = null
    ) {
        $this->validator = $validator;
        $this->discountCodes = $discountCodes;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setDescription('Generated unique codes, decide length and quantity.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $lengthQuestion = new Question('Please write length: ');
        $length = $helper->ask($input, $output, $lengthQuestion);

        $this->validateQuestion($length, $io);

        $quantityQuestion = new Question('Please write quantity: ');
        $quantity = $helper->ask($input, $output, $quantityQuestion);

        $this->validateQuestion($quantity, $io);

        $results = $this->discountCodes->generatedDiscountCodes($length, $quantity);

        if ($results) {
            $file = fopen("codes.txt", "w");
            fwrite($file, $results);

            $io->success('Create file with discount codes in file: codes.txt.');
        }

        return 0;
    }

    private function validateQuestion(int $number,SymfonyStyle $io)
    {
        $errors = $this->validator->validate($number, [
            new Type('numeric'),
            new GreaterThan(0)
        ]);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $io->error($error->getMessage());
            }

            return 0;
        }
    }

}