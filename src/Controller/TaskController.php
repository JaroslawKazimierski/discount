<?php

namespace App\Controller;

use App\Model\Task;
use App\Form\Type\TaskType;
use App\Service\DiscountCodes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @param DiscountCodes $discountCodes
     * @return Response
     */
    public function calculate(Request $request, DiscountCodes $discountCodes): Response
    {
        /**@var Task $task */
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $generatedCodes = $discountCodes->generatedDiscountCodes($task->getLength(), $task->getQuantity());

            return $this->render('codes.html.twig', [
                'codes' => $generatedCodes
            ]);
        }

        return $this->render('task/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
