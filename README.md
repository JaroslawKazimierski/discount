# Requirements
- php 7.1.3
- composer

# Setup
- run `composer install`
- create .env and copy .env.dist to .env

# Run
- run `bin/console server:run` to get it running on http://localhost:8000

# Run Command
- run `bin/console app:generated-codes`